using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// https://catlikecoding.com/unity/tutorials/movement/orbit-camera/

[RequireComponent(typeof(Camera))]
public class OrbitCameraController : MonoBehaviour
{
    public Transform Target;
    public float TargetDist = 10;

    public float FocusRadius = 0f;

    public Vector2 OrbitAngles = new Vector2(45f, 0f);
    [SerializeField, Range(1f, 360f)]
    float verticalRotationSpeed = 180f;
    [SerializeField, Range(1f, 360f)]
    float horizontalRotationSpeed = 270f;

    [SerializeField, Range(-89f, 89f)]
    float minVerticalAngle = 15f, maxVerticalAngle = 75f;

    private Vector3 m_FocusPosition;
    private Vector3 m_PreviousMousePosition;
    private Camera m_Camera;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();

        m_Camera.transform.LookAt(Target);
        m_FocusPosition = Target.position;

        m_PreviousMousePosition = Input.mousePosition;
    }

    // For unity editor 
    void OnValidate()
    {
        if (maxVerticalAngle < minVerticalAngle)
        {
            maxVerticalAngle = minVerticalAngle;
        }
    }

    void LateUpdate()
    {
        // Manual
        if (Input.GetMouseButton(1))
        {
            Vector3 mousePosition = Input.mousePosition;
            Vector3 delta = m_PreviousMousePosition - mousePosition;

            float horizontalDirection = delta.x == 0 ? 0 : delta.x > 0 ? 1 : -1;
            float verticalDirection = delta.y == 0 ? 0 : delta.y > 0 ? 1 : -1;

            //Vector2 input = new Vector2(verticalDirection, -horizontalDirection);

            //Vector2 input = new Vector2(
            //    Input.GetAxis("Vertical Camera"),
            //    Input.GetAxis("Horizontal Camera")
            //);

            //const float e = 0.001f;
            //if (verticalDirection.x < -e || input.x > e || input.y < -e || input.y > e)
            //{
            OrbitAngles += new Vector2(-verticalRotationSpeed * Time.unscaledDeltaTime * verticalDirection, horizontalRotationSpeed * Time.unscaledDeltaTime * horizontalDirection);

            OrbitAngles.x = Mathf.Clamp(OrbitAngles.x, minVerticalAngle, maxVerticalAngle);

            if (OrbitAngles.y < 0f)
                OrbitAngles.y += 360f;

            else if (OrbitAngles.y >= 360f)
                OrbitAngles.y -= 360f;
        }

        if (Input.mouseScrollDelta != Vector2.zero)
        {
            Debug.Log(Input.mouseScrollDelta);
            if (Input.mouseScrollDelta.y > 0)
            {
                TargetDist -= 0.5f;
            }
            else
            {
                TargetDist += 0.5f;
            }
        }

        //if (delta.x > delta.y)
        //{
        //    float horizontalDirection = delta.x == 0 ? 0 : delta.x > 0 ? 1 : -1;
        //    transform.RotateAround(Target.transform.position, Vector3.up, RotationSpeed * horizontalDirection);
        //}
        //else
        //{
        //    float verticalDirection = delta.y == 0 ? 0 : delta.y > 0 ? 1 : -1;
        //    transform.RotateAround(Target.transform.position, m_Camera.transform.right, RotationSpeed * verticalDirection);
        //}

        // -- Auto ------------------------------

        // Keep the right distance between camera and target
        if (FocusRadius > 0f)
        {
            float distance = Vector3.Distance(Target.position, m_FocusPosition);
            if (distance > FocusRadius)
            {
                m_FocusPosition = Vector3.Lerp(
                    Target.position, m_FocusPosition, FocusRadius / distance
                );
            }
        }
        else
        {
            m_FocusPosition = Target.position;
        }

        Quaternion lookRotation = Quaternion.Euler(OrbitAngles);
        Vector3 lookDirection = lookRotation * Vector3.forward;

        Vector3 lookPosition = m_FocusPosition - lookDirection * TargetDist;

        transform.SetPositionAndRotation(lookPosition, lookRotation);

        //transform.position = m_FocusPosition - (m_Camera.transform.forward * TargetDist);


        //m_Camera.transform.LookAt(Target);

        m_PreviousMousePosition = Input.mousePosition;
    }
}
