using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private Vector3Int m_Coordinate = Vector3Int.zero;
    public Vector3Int Coordinate
    {
        get
        {
            // dynamic position 
            m_Coordinate = transform.position.ToTileCoord();

            return m_Coordinate;
        }
    }

    public bool IsHighlighted { get; private set; } = false;

    private Highlight Highlight;



    private void Awake()
    {
        Highlight = GetComponent<Highlight>();
    }

    public void SetHighlight(bool enabled)
    {
        if (enabled == IsHighlighted) return;

        if (enabled)
            Highlight.Show();
        else
            Highlight.Hide();

        IsHighlighted = enabled;
    }
}
