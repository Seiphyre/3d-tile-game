using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AppManager : Singleton<AppManager>
{
    public GameObject NodePrefab;
    public GameObject NavmeshParent;

    public Navmesh Navmesh { get; private set; }
    //private NavmeshPath m_Path;

    private List<GameObject> m_DisplayedNodes = new List<GameObject>();

    protected override void Init()
    {
        Debug.Log("ICI");
        Navmesh = NavmeshBuilder.GenerateNavmesh(World.Instance.TilesMap);

        if (Navmesh != null)
        {
            DisplayNavmash();
        }
    }

    public void DisplayNavmash()
    {
        // Clear previous displayed nodes
        if (m_DisplayedNodes != null && m_DisplayedNodes.Count > 0)
        {
            foreach (GameObject node in m_DisplayedNodes)
                Destroy(node);

            m_DisplayedNodes.Clear();
        }

        // Display
        foreach (NavmeshNode node in Navmesh.Nodes.Values)
        {
            GameObject Node = Instantiate(NodePrefab, node.Coord, Quaternion.identity);
            Node.transform.parent = NavmeshParent.transform;

            foreach (NavmeshNode neighborNode in node.Neighbors)
            {
                Color color = Color.yellow;
                if (neighborNode.Coord.y != node.Coord.y)
                    color = Color.red;

                Debug.DrawLine(node.Coord, neighborNode.Coord, color, 60);
            }

            m_DisplayedNodes.Add(Node);
        }
    }

    //public void DisplayPath()
    //{
    //    // Reset color of all displayed nodes
    //    foreach (GameObject node in m_DisplayedNodes)
    //        node.GetComponent<Renderer>().material.color = NodePrefab.GetComponent<Renderer>().sharedMaterial.color;

    //    // Set red color to path nodes
    //    foreach (NavmeshNode node in m_Path.Nodes)
    //    {
    //        GameObject pathNode = m_DisplayedNodes.FirstOrDefault(o => o.transform.position == node.Coord);

    //        if (pathNode != null)
    //            pathNode.GetComponent<Renderer>().material.color = Color.red;
    //    }
    //}
}
