using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavmeshNode
{
    public Vector3Int Coord = Vector3Int.zero;
    public int Cost = -1;

    public List<NavmeshNode> Neighbors = new List<NavmeshNode>();


    // -------------------------------------------

    public NavmeshNode() { }

    public NavmeshNode(Vector3Int coord)
    {
        Coord = coord;
    }
}
