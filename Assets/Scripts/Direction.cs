using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Unknown = 0,

    North,
    South,
    West,
    East
}
