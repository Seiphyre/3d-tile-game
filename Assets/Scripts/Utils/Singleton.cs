using UnityEngine;

/// 
/// Inherit from this base class to create a singleton.
/// e.g. public class MyClassName : Singleton {}
/// 
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T m_Instance;

    /// 
    /// Access singleton instance through this propriety.
    /// 
    public static T Instance
    {
        get
        {
            if (m_Instance == null)
            {

                // Search for existing instance.
                m_Instance = FindObjectOfType<T>();

                // Create new instance if one doesn't already exist.
                if (m_Instance == null)
                {
                    // Need to create a new GameObject to attach the singleton to.
                    var singletonObject = new GameObject();
                    m_Instance = singletonObject.AddComponent<T>();
                    singletonObject.name = typeof(T).ToString() + " (Singleton)";

                    m_Instance.Init();

                    // Make instance persistent.
                    DontDestroyOnLoad(singletonObject);
                }
            }

            return m_Instance;
        }
    }

    protected virtual void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = GetComponent<T>();
            m_Instance.Init();

            DontDestroyOnLoad(this);
        }
    }

    protected virtual void Init() { }
}
