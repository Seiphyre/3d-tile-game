using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extension 
{
    public static Vector3Int ToTileCoord(this Vector3 vector)
    {
        return new Vector3Int(
            Mathf.FloorToInt(vector.x), 
            Mathf.FloorToInt(vector.y), 
            Mathf.FloorToInt(vector.z)
            );
    }

}
