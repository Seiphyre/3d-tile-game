using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int MovementPoints = 5;
    private NavmeshAgent m_NavmeshAgent;
    private List<Tile> m_HighlightedTiles = new List<Tile>();
    private void Start()
    {
        m_NavmeshAgent = GetComponent<NavmeshAgent>();

        m_NavmeshAgent.Navmesh = AppManager.Instance.Navmesh;
        ShowMovementRange();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                Tile target = World.Instance.GetTile(hit.transform.position.ToTileCoord());

                if (target && target.IsHighlighted)
                {
                    Vector3Int destination = target.Coordinate + Vector3Int.up;

                    bool isValidDestination = m_NavmeshAgent.Move(destination);

                    if (isValidDestination)
                        m_NavmeshAgent.OnReachDestination += OnReachDestination;
                }
            }
        }
    }

    private void OnReachDestination()
    {
        ShowMovementRange();
        m_NavmeshAgent.OnReachDestination -= OnReachDestination;
    }

    private void ShowMovementRange()
    {
        if (m_HighlightedTiles.Count > 0)
        {
            foreach (Tile tile in m_HighlightedTiles)
            {
                tile.SetHighlight(false);
            }

            m_HighlightedTiles.Clear();
        }

        m_NavmeshAgent.Navmesh.CalculateNodeCost(transform.position.ToTileCoord(), MovementPoints);
        List<NavmeshNode> nodes = m_NavmeshAgent.Navmesh.Nodes.Values.ToList().FindAll(n => n.Cost > 0 && n.Cost <= MovementPoints);

        foreach(NavmeshNode node in nodes)
        {
            Vector3Int tileCoord = node.Coord + Vector3Int.down;

            World.Instance.TilesMap[tileCoord].SetHighlight(true);

            m_HighlightedTiles.Add(World.Instance.TilesMap[tileCoord]);
        }

        //foreach (NavmeshNode node in m_NavmeshAgent.Navmesh.)
    }
}
