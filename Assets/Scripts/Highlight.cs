using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour
{
    private Dictionary<Renderer, Material[]> m_HighlightMaterialDict = new Dictionary<Renderer, Material[]>();
    private Dictionary<Renderer, Material[]> m_OriginalMaterialDict = new Dictionary<Renderer, Material[]>();
    private Dictionary<Color, Material> m_CachedHighlightMaterials = new Dictionary<Color, Material>();

    public Material HighlightMaterial;

    private bool IsHighlighted = false;

    private void Awake()
    {
        PrepareMaterialDict();
    }

    public void Show()
    {
        if (IsHighlighted) return;

        foreach (Renderer renderer in m_OriginalMaterialDict.Keys)
        {
            renderer.materials = m_HighlightMaterialDict[renderer];
        }

        IsHighlighted = true;
    }

    public void Hide()
    {
        if (!IsHighlighted) return;

        foreach (Renderer renderer in m_OriginalMaterialDict.Keys)
        {
            renderer.materials = m_OriginalMaterialDict[renderer];
        }

        IsHighlighted = false;
    }

    private void PrepareMaterialDict()
    {
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            Material[] originalMaterials = renderer.materials;
            m_OriginalMaterialDict.Add(renderer, originalMaterials);

            Material[] newMaterials = new Material[originalMaterials.Length];
            for (int i = 0; i < originalMaterials.Length; i++)
            {

                Material mat = null;
                if (m_CachedHighlightMaterials.TryGetValue(originalMaterials[i].color, out mat) == false)
                {
                    mat = new Material(HighlightMaterial);
                    mat.color = originalMaterials[i].color;
                    mat.mainTexture = originalMaterials[i].mainTexture;

                    m_CachedHighlightMaterials.Add(mat.color, mat);
                }
                newMaterials[i] = mat;
            }
            m_HighlightMaterialDict.Add(renderer, newMaterials);
        }
    }
}
