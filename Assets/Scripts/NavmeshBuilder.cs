using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class NavmeshBuilder
{
    //public static void GenerateNavmesh()
    //{
    //    GenerateNavmesh(World.Instance.TilesMap);
    //}

    //public static void GenerateNavmeshFromEditor()
    //{
    //    Tile[] allTiles = FindObjectsOfType<Tile>();
    //    Dictionary<Vector3Int, Tile> tilesMap = new Dictionary<Vector3Int, Tile>();

    //    foreach (Tile tile in allTiles)
    //    {
    //        if (tilesMap.ContainsKey(tile.Coordinate))
    //        {
    //            Debug.LogWarning($"{tilesMap[tile.Coordinate].name} and {tile.name} have the same coordinates. {tile.name} will be ignored.");
    //            continue;
    //        }

    //        tilesMap.Add(tile.Coordinate, tile);
    //    }

    //    GenerateNavmesh(tilesMap);
    //}

    public static Navmesh GenerateNavmesh(Dictionary<Vector3Int, Tile> tilesMap)
    {
        Debug.Log("Generate Navmesh");

        Navmesh navmesh = new Navmesh();

        // Walkable nodes
        foreach (Tile tile in tilesMap.Values)
        {
            Vector3Int tileCoord = tile.Coordinate;
            Vector3Int upperTileCoord = tile.Coordinate + new Vector3Int(0, 1, 0);

            // Ignore tiles under other tiles
            if (tilesMap.ContainsKey(upperTileCoord))
                continue;

            Vector3Int nodeCoord = new Vector3Int(tile.Coordinate.x, tile.Coordinate.y + 1, tile.Coordinate.z);

            navmesh.Nodes.Add(nodeCoord, new NavmeshNode(nodeCoord));
        }


        // Compute neighbors
        foreach (NavmeshNode node in navmesh.Nodes.Values)
        {
            NavmeshNode neighbor;

            // North
            neighbor = navmesh.GetNode(node.Coord.x + 1, node.Coord.y, node.Coord.z);

            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // East
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y, node.Coord.z + 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // South
            neighbor = navmesh.GetNode(node.Coord.x - 1, node.Coord.y, node.Coord.z);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // West
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y, node.Coord.z - 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Jump North
            neighbor = navmesh.GetNode(node.Coord.x + 1, node.Coord.y + 1, node.Coord.z);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);


            // Jump East
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y + 1, node.Coord.z + 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Jump South
            neighbor = navmesh.GetNode(node.Coord.x - 1, node.Coord.y + 1, node.Coord.z);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Jump West
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y + 1, node.Coord.z - 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Fall North
            neighbor = navmesh.GetNode(node.Coord.x + 1, node.Coord.y - 1, node.Coord.z);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Fall East
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y - 1, node.Coord.z + 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Fall South
            neighbor = navmesh.GetNode(node.Coord.x - 1, node.Coord.y - 1, node.Coord.z);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

            // Fall West
            neighbor = navmesh.GetNode(node.Coord.x, node.Coord.y - 1, node.Coord.z - 1);
            if (neighbor != null)
                node.Neighbors.Add(neighbor);

        }

        return navmesh;
    }
}
