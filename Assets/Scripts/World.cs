using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class World : Singleton<World>
{
    private Dictionary<Vector3Int, Tile> _tiles = new Dictionary<Vector3Int, Tile>();
    public Dictionary<Vector3Int, Tile> TilesMap { get => _tiles; }

    protected override void Init()
    {
        base.Init();

        Debug.Log("Setup world");
        FindAndGetAllTiles();
    }

    private void FindAndGetAllTiles()
    {
        Tile[] allTiles = FindObjectsOfType<Tile>();

        foreach (Tile tile in allTiles)
        {
            if (_tiles.ContainsKey(tile.Coordinate))
            {
                Debug.LogWarning($"{_tiles[tile.Coordinate].name} and {tile.name} have the same coordinates. {tile.name} will be ignored.");
                continue;
            }

            _tiles.Add(tile.Coordinate, tile);
        }
    }

    public Tile GetTile(Vector3Int coord)
    {
        return GetTile(coord.x, coord.y, coord.z);
    }

    public Tile GetTile(int x, int y, int z)
    {
        Vector3Int coordinate = new Vector3Int(x, y, z);

        if (!_tiles.ContainsKey(coordinate))
            return null;

        if (_tiles[coordinate] == null)
            return null;

        return _tiles[coordinate];
    }

    public List<Tile> GetAllTiles()
    {
        return _tiles.Values.ToList();
    }
}
