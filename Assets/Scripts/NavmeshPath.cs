using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NavmeshPath
{
    public List<NavmeshNode> Nodes { get; private set; } = new List<NavmeshNode>();

    public Vector3Int? Destination => Nodes?.Last()?.Coord;
    public Vector3Int? Beginning => Nodes?.First()?.Coord;

    // ------------------------------------------------

    public NavmeshPath () { }

    public NavmeshPath (List<NavmeshNode> nodes)
    {
        Nodes = nodes;
    }
}
