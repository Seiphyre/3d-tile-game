//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class GridBehaviour : MonoBehaviour
//{
//    public bool GeneratePath = false;

//    public int Rows = 10;
//    public int Columns = 10;
//    public int Scale = 1;

//    public GameObject TilePrefab;
//    public Vector3 LeftBottomLocation;

//    public GameObject[,] Tiles;

//    public Vector2 Start = Vector2.zero;
//    public Vector2 End = new Vector2(2, 2);

//    public Color PathColor = Color.red;
//    public Color TileColor = Color.yellow;
//    public Color StartColor = Color.green;

//    public List<GameObject> Path = new List<GameObject>();

//    // Start is called before the first frame update
//    void Awake()
//    {
//        Tiles = new GameObject[Columns, Rows];

//        if (TilePrefab)
//            GenerateGrid();
//        else Debug.LogWarning("Grid prefab is missing.");
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (GeneratePath)
//        {
//            ComputeTilesDistance();
//            ComputePath();

//            foreach (GameObject obj in Path)
//                obj.GetComponent<Renderer>().material.color = PathColor;

//            GeneratePath = false;
//        }
//    }



//    // -------------------------------------------------------

//    void GenerateGrid()
//    {
//        for (int i = 0; i < Columns; i++)
//        {
//            for (int j = 0; j < Rows; j++)
//            {
//                GameObject tile = Instantiate(TilePrefab, new Vector3(LeftBottomLocation.x + Scale * i, LeftBottomLocation.y, LeftBottomLocation.z + Scale * j), Quaternion.identity);

//                tile.transform.SetParent(this.transform);
//                tile.GetComponent<Node>().X = i;
//                tile.GetComponent<Node>().Y = j;

//                Tiles[i, j] = tile;
//            }
//        }
//    }



//    // -------------------------------------------------------

//    void InitTiles()
//    {
//        foreach (GameObject tilePrefab in Tiles)
//        {
//            if (tilePrefab == null)
//                continue;

//            Node tile = tilePrefab.GetComponent<Node>();
//            Renderer renderer = tilePrefab.GetComponent<Renderer>();

//            if (tile.X == Start.x && tile.Y == Start.y)
//            {
//                tile.Distance = 0;
//                renderer.material.color = StartColor;
//            }
//            else
//            {
//                tile.Distance = -1;
//                renderer.material.color = TileColor;
//            }
//        }
//    }



//    void ComputeTilesDistance()
//    {
//        InitTiles();

//        int x = (int)Start.x;
//        int y = (int)Start.y;
//        int maxDistance = Rows * Columns;

//        for (int distance = 1; distance < maxDistance; distance++)
//        {
//            foreach (GameObject tilePrefab in Tiles)
//            {
//                // Ignore empty tiles
//                if (tilePrefab == null) continue;

//                Node tile = tilePrefab.GetComponent<Node>();

//                if (tile.Distance == distance - 1)
//                {
//                    SetDistanceEmptyNeighbors(tile.X, tile.Y, distance);
//                }
//            }
//        }
//    }

//    void SetDistanceEmptyNeighbors(int x, int y, int distance)
//    {
//        // North neighbor
//        if (GetNeighborTile(x, y, Direction.North)?.Distance == -1)
//            SetDistance(x, y + 1, distance);

//        // East neighbor
//        if (GetNeighborTile(x, y, Direction.East)?.Distance == -1)
//            SetDistance(x + 1, y, distance);

//        // South neighbor
//        if (GetNeighborTile(x, y, Direction.South)?.Distance == -1)
//            SetDistance(x, y - 1, distance);

//        // West neighbor
//        if (GetNeighborTile(x, y, Direction.West)?.Distance == -1)
//            SetDistance(x - 1, y, distance);
//    }

//    void SetDistance(int x, int y, int distance)
//    {
//        if (Tiles[x, y])
//        {
//            Tiles[x, y].GetComponent<Node>().Distance = distance;
//        }
//    }



//    public Node GetTile(int x, int y)
//    {
//        if (IsOutOfBound(x, y))
//            return null;

//        if (Tiles[x, y] == null)
//            return null;

//        return Tiles[x, y].GetComponent<Node>();
//    }

//    public Node GetNeighborTile(int x, int y, Direction neighborDirection)
//    {
//        Node neighborTile = null;

//        switch (neighborDirection)
//        {
//            case Direction.North:
//                neighborTile = GetTile(x, y + 1);
//                break;

//            case Direction.East:
//                neighborTile = GetTile(x + 1, y);
//                break;

//            case Direction.South:
//                neighborTile = GetTile(x, y - 1);
//                break;

//            case Direction.West:
//                neighborTile = GetTile(x - 1, y);
//                break;
//        }

//        return neighborTile;
//    }

//    public bool IsOutOfBound(int x, int y)
//    {
//        if (x >= Columns) return true;
//        if (x < 0) return true;

//        if (y >= Rows) return true;
//        if (y < 0) return true;

//        return false;
//    }



//    // -------------------------------------------------------

//    void ComputePath()
//    {
//        int endDistance;

//        // Clear previous path
//        Path.Clear();

//        // Start computing the path from the end of the path
//        if (Tiles[(int)End.x, (int)End.y] && Tiles[(int)End.x, (int)End.y].GetComponent<Node>().Distance != -1)
//        {
//            Path.Add(Tiles[(int)End.x, (int)End.y]);
//            endDistance = Tiles[(int)End.x, (int)End.y].GetComponent<Node>().Distance;
//        }
//        else
//        {
//            Debug.Log("Cannot reach the end...");
//            return;
//        }

//        // Computer the path
//        List<GameObject> nextTiles = new List<GameObject>();
//        int x = (int)End.x;
//        int y = (int)End.y;

//        for (int distance = endDistance; distance > 1; distance--)
//        {
//            if (GetNeighborTile(x, y, Direction.North)?.Distance == distance - 1)
//                nextTiles.Add(Tiles[x, y + 1]);

//            if (GetNeighborTile(x, y, Direction.East)?.Distance == distance - 1)
//                nextTiles.Add(Tiles[x + 1, y]);

//            if (GetNeighborTile(x, y, Direction.South)?.Distance == distance - 1)
//                nextTiles.Add(Tiles[x, y - 1]);

//            if (GetNeighborTile(x, y, Direction.West)?.Distance == distance - 1)
//                nextTiles.Add(Tiles[x - 1, y]);

//            GameObject closestTile = FindClosest(Tiles[(int)End.x, (int)End.y].transform, nextTiles);
//            Path.Add(closestTile);

//            x = closestTile.GetComponent<Node>().X;
//            y = closestTile.GetComponent<Node>().Y;
//            nextTiles.Clear();
//        }
//    }

//    GameObject FindClosest(Transform targetLocation, List<GameObject> list)
//    {
//        if (list == null || list.Count == 0) return null;

//        float currentDistance = Scale * Rows * Columns;
//        int indexNumber = 0;

//        for (int i = 0; i < list.Count; i++)
//        {
//            if (Vector3.Distance(targetLocation.position, list[i].transform.position) < currentDistance)
//            {
//                currentDistance = Vector3.Distance(targetLocation.position, list[i].transform.position);
//                indexNumber = i;
//            }
//        }

//        return list[indexNumber];
//    }
//}
