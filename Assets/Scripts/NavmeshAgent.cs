using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavmeshAgent : MonoBehaviour
{
    public float Speed = 5f;

    public Navmesh Navmesh { get; set; }

    private NavmeshPath m_Path;
    private int m_PathIndex = -1;

    public bool IsMoving { get; private set; } = false;

    public Action OnReachDestination;



    // ------------------------------------------


    private void Update()
    {
        if (IsMoving)
        {
            // Update destination
            if (Vector3.Distance(transform.position, m_Path.Nodes[m_PathIndex].Coord) < 0.01f)
            {
                transform.position = m_Path.Nodes[m_PathIndex].Coord;

                if (IsLastPathIndex())
                {
                    Debug.Log("On s'arrete !");
                    IsMoving = false;

                    OnReachDestination?.Invoke();

                    return;
                }
                else
                {
                    Debug.Log("On continue !");
                    m_PathIndex = GetNextPathIndex();
                }
            }

            // Move
            Vector3 direction = m_Path.Nodes[m_PathIndex].Coord - transform.position;
            direction.Normalize();

            transform.position += direction * Time.deltaTime * Speed;
        }
    }

    // -------------------------------------------

    public bool SetDestination(Vector3Int destination)
    {
        Vector3Int beginning = transform.position.ToTileCoord();
        bool result = true;

        result = Navmesh.CalculateNodeCost(beginning);

        if (!result)
            return false;

        m_Path = Navmesh.CalculatePath(beginning, destination);
        m_PathIndex = 0;

        return m_Path != null;
    }

    public bool Move(Vector3Int destination)
    {
        if (IsMoving)
            return false;

        bool IsValidDestination = SetDestination(destination);

        if (!IsValidDestination)
            return false;

        IsMoving = true;

        return true;
    }

    public bool IsLastPathIndex()
    {
        return m_PathIndex == m_Path.Nodes.Count - 1;
    }

    public int GetNextPathIndex()
    {
        return Mathf.Min(m_PathIndex + 1, m_Path.Nodes.Count - 1);
    }
}
