using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Navmesh
{
    public Dictionary<Vector3Int, NavmeshNode> Nodes = new Dictionary<Vector3Int, NavmeshNode>();



    public bool CalculateNodeCost(Vector3Int start, int range = -1)
    {
        // Reset Cost
        foreach (NavmeshNode node in Nodes.Values)
        {
            node.Cost = -1;
        }

        if (!Nodes.ContainsKey(start))
        {
            Debug.Log("The start of the path is out of navmesh boundary.");
            return false;
        }

        // Set the starting point
        Nodes[start].Cost = 0;

        int maxCost = range < 0 ? Nodes.Values.Count - 1 : range;

        for (int cost = 1; cost <= maxCost; cost++)
        {
            foreach (NavmeshNode node in Nodes.Values)
            {
                //// Ignore empty node
                //if (node == null) continue;

                if (node.Cost == cost - 1)
                {
                    foreach (NavmeshNode neighbor in node.Neighbors)
                    {
                        if (neighbor.Cost == -1)
                            neighbor.Cost = cost;
                    }
                }
            }
        }

        return true;
    }

    public NavmeshPath CalculatePath(Vector3Int start, Vector3Int end)
    {
        NavmeshNode endNode = GetNode(end.x, end.y, end.z);
        if (endNode == null)
        {
            Debug.Log("The end of the path is out of navmesh boundary.");
            return null;
        }

        NavmeshNode startNode = GetNode(start.x, start.y, start.z);
        if (startNode == null)
        {
            Debug.Log("The start of the path is out of navmesh boundary.");
            return null;
        }

        if (endNode.Cost == -1)
        {
            Debug.Log("The end of the path is not reachable.");
            return null;
        }

        List<NavmeshNode> pathNodes = new List<NavmeshNode>() { endNode };

        NavmeshNode currentNode = endNode;
        List<NavmeshNode> closeNeighbors = new List<NavmeshNode>();
        for (int step = endNode.Cost; step > 1; step--)
        {
            closeNeighbors = currentNode.Neighbors.FindAll(n => n.Cost == step - 1);
            NavmeshNode closestNeighbor = FindClosest(start, closeNeighbors);

            pathNodes.Add(closestNeighbor);

            currentNode = closestNeighbor;
        }

        pathNodes.Reverse();

        return new NavmeshPath(pathNodes);
    }

    NavmeshNode FindClosest(Vector3Int targetLocation, List<NavmeshNode> nodes)
    {
        if (nodes == null || nodes.Count == 0) return null;

        float currentDistance = nodes.First().Cost;
        int indexNumber = 0;

        for (int i = 1; i < nodes.Count; i++)
        {
            float distance = Vector3.Distance(targetLocation, nodes[i].Coord);
            if (distance < currentDistance)
            {
                currentDistance = distance;
                indexNumber = i;
            }
        }

        return nodes[indexNumber];
    }



    public NavmeshNode GetNode(int x, int y, int z)
    {
        Vector3Int coordinate = new Vector3Int(x, y, z);

        if (!Nodes.ContainsKey(coordinate))
            return null;

        if (Nodes[coordinate] == null)
            return null;

        return Nodes[coordinate];
    }
}
